module.exports = class DefaultApiResponse {
    constructor () {
        this.success = false;
        this.type = '';
        this.title = '';
        this.message = '';
        this.data = [];
    }
}