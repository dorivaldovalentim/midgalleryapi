const connection = require('./../database/connection');

module.exports = class Midia {

    static get(callback) {
        return connection.query("SELECT * FROM midias", callback);
    }

    static store(request, callback) {
        return connection.query(
            "INSERT INTO midias (title, description, type, path) VALUES (?, ?, ?, ?)",
            [
                request.title,
                request.description,
                request.type,
                request.path
            ],
        callback);
    }

    static getById(id, callback) {
        return connection.query("SELECT * FROM midias WHERE id = ?", [ id ], callback);
    }

    static update(id, request, callback) {

        if (request.path != null) {
            return connection.query(
                "UPDATE midias SET title = ?, description = ?, type = ?, path = ? WHERE id = ?",
                [
                    request.title,
                    request.description,
                    request.type,
                    request.path,
                    id
                ],
            callback);
        } else {
            return connection.query(
                "UPDATE midias SET title = ?, description = ?, type = ? WHERE id = ?",
                [
                    request.title,
                    request.description,
                    request.type,
                    id
                ],
            callback);
        }
        
    }

    static delete(id, callback) {
        return connection.query("DELETE FROM midias WHERE id = ?", [id], callback);
    }

}