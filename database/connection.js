const mysql = require('mysql');
 
let connection = mysql.createPool({
    host: 'localhost',
    database: 'midgallery',
    user: 'root',
    password: ''
});

module.exports = connection;