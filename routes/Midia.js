const express = require('express');
const router = express.Router();
const multer = require('multer');
const path = require('path');

const Midia = require('./../Models/Midia');
const DefaultApiResponse = require('./../Models/DefaultApiResponse');
const fs = require('fs');

let public_folder = './public/midias/';

let storage = multer.diskStorage({
    destination: function(request, file, cb) {
        cb(null, public_folder);
    },

    filename: function(request, file, cb) {
        let archive_name = `${file.fieldname.replace(/\//g, '')}-${Date.now()}${path.extname(file.originalname)}`;

        request.body.path = public_folder + archive_name;
        cb(null, archive_name);
    }
});

var upload = multer({ storage: storage });

function delete_file(file) {
    if (file != null) {
        fs.unlinkSync(file);
    }
}

router.get('/', function(request, response, next) {
    Midia.get(function(error, mysqlresponse) {
        let apiresponse = new DefaultApiResponse();

        if (error) {
            apiresponse.success = false;
            apiresponse.type = 'danger';
            apiresponse.title = 'Erro';
            apiresponse.message = 'Ocorreu algum erro';
        } else {
            apiresponse.success = true;
            apiresponse.type = 'success';
            apiresponse.title = 'Sucesso';
            apiresponse.message = 'Sucesso';
            apiresponse.data = mysqlresponse;
         }

        response.json(apiresponse);
    });
});

router.post('/store', upload.single('file'), function (request, response, next) {
    let apiresponse = new DefaultApiResponse();

    if (request.file == null) {
        apiresponse.success = false;
        apiresponse.type = 'warning';
        apiresponse.title = 'Aviso';
        apiresponse.message = 'Você precisa enviar alguma mídia';
        response.json(apiresponse);
        return;
    }

    Midia.store(request.body, function (error, mysqlresponse) {

        if (error) {
            apiresponse.success = false;
            apiresponse.type = 'error';
            apiresponse.title = 'Erro';
            apiresponse.message = 'Ocorreu algum erro';
            delete_file(request.body.path);
        } else {

            if (mysqlresponse.affectedRows > 0) {
                apiresponse.success = true;
                apiresponse.type = 'success';
                apiresponse.title = 'Sucesso';
                apiresponse.message = 'Mídia cadastrada com sucesso';
                apiresponse.data = [];
            } else {
                apiresponse.success = false;
                apiresponse.type = 'error';
                apiresponse.title = 'Erro';
                apiresponse.message = 'Erro ao cadastrar mídia';
                apiresponse.data = mysqlresponse;
                delete_file(request.body.path);
            }

        }

        response.json(apiresponse);
    });
});

router.get('/:id?', function (request, response, next) {
    Midia.getById(request.params.id, function (error, mysqlresponse) {
        let apiresponse = new DefaultApiResponse();

        if (error) {
            apiresponse.success = false;
            apiresponse.type = 'danger';
            apiresponse.title = 'Erro';
            apiresponse.message = 'Ocorreu algum erro';
        } else {
            apiresponse.success = true;
            apiresponse.type = 'success';
            apiresponse.title = 'Sucesso';
            apiresponse.message = 'Sucesso';
            apiresponse.data = mysqlresponse;
        }

        response.json(apiresponse);
    });
});

router.put('/update/:id', upload.single('file'), function (request, response, next) {
    let apiresponse = new DefaultApiResponse();

    Midia.update(request.params.id, request.body, function (error, mysqlresponse) {

        if (error) {
            apiresponse.success = false;
            apiresponse.type = 'error';
            apiresponse.title = 'Erro';
            apiresponse.message = 'Ocorreu algum erro';
            delete_file(request.body.path);
        } else {

            if (mysqlresponse.affectedRows > 0) {
                apiresponse.success = true;
                apiresponse.type = 'success';
                apiresponse.title = 'Sucesso';
                apiresponse.message = 'Mídia actualizada com sucesso';
                apiresponse.data = [];
            } else {
                apiresponse.success = false;
                apiresponse.type = 'error';
                apiresponse.title = 'Erro';
                apiresponse.message = 'Erro ao actualizar mídia';
                apiresponse.data = mysqlresponse;
                delete_file(request.body.path);
            }

        }

        response.json(apiresponse);
    });
});

router.delete('/delete/:id', function (request, response, next) {
    Midia.delete(request.params.id, function (error, mysqlresponse) {
        let apiresponse = new DefaultApiResponse();

        if (error) {
            apiresponse.success = false;
            apiresponse.type = 'error';
            apiresponse.title = 'Erro';
            apiresponse.message = 'Ocorreu algum erro';
        } else {

            if (mysqlresponse.affectedRows > 0) {
                apiresponse.success = true;
                apiresponse.type = 'success';
                apiresponse.title = 'Sucesso';
                apiresponse.message = 'Mídia apagada com sucesso';
                apiresponse.data = [];
            } else {
                apiresponse.success = false;
                apiresponse.type = 'error';
                apiresponse.title = 'Erro';
                apiresponse.message = 'Erro ao apagar mídia';
                apiresponse.data = [];
                delete_file(request.body.path);
            }

        }

        response.json(apiresponse);
    });
});

module.exports = router;