const express = require('express');
const bodyparser = require('body-parser');
const cors = require('cors');
const port = 3000;
const main = express();
const router = express.Router();
const midiaRouter = require('./routes/Midia');

main.use(cors());

main.use(bodyparser.urlencoded({ extended: true }));
main.use(bodyparser.urlencoded({ limit: '20mb', extended: true }));
main.use('/public', express.static(__dirname + '/public'));

router.get('/', (request, response) => {
    response.json({ message: '=> API Online 😉' });
});

main.use('/', router);
main.use('/midias', midiaRouter);
main.listen(port);
console.log('API Express Running')